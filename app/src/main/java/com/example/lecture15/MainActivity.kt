package com.example.lecture15

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.core.content.withStyledAttributes
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.card_view.view.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    lateinit var adapter:Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setup()

    }

    private fun setup(){

         var model:MyModel= MyModel()
         var admodel:MyModel.ad=MyModel.ad()
         var datalist= mutableListOf<MyModel.data>()
         var adlist= mutableListOf<MyModel.ad>()


         val jsonstring="// 20200506142909\n" +
                "// https://reqres.in/api/users?page=2\n" +
                "\n" +
                "{\n" +
                "  \"page\": 2,\n" +
                "  \"per_page\": 6,\n" +
                "  \"total\": 12,\n" +
                "  \"total_pages\": 2,\n" +
                "  \"data\": [\n" +
                "    {\n" +
                "      \"id\": 7,\n" +
                "      \"email\": \"michael.lawson@reqres.in\",\n" +
                "      \"first_name\": \"Michael\",\n" +
                "      \"last_name\": \"Lawson\",\n" +
                "      \"avatar\": \"https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 8,\n" +
                "      \"email\": \"lindsay.ferguson@reqres.in\",\n" +
                "      \"first_name\": \"Lindsay\",\n" +
                "      \"last_name\": \"Ferguson\",\n" +
                "      \"avatar\": \"https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 9,\n" +
                "      \"email\": \"tobias.funke@reqres.in\",\n" +
                "      \"first_name\": \"Tobias\",\n" +
                "      \"last_name\": \"Funke\",\n" +
                "      \"avatar\": \"https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 10,\n" +
                "      \"email\": \"byron.fields@reqres.in\",\n" +
                "      \"first_name\": \"Byron\",\n" +
                "      \"last_name\": \"Fields\",\n" +
                "      \"avatar\": \"https://s3.amazonaws.com/uifaces/faces/twitter/russoedu/128.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 11,\n" +
                "      \"email\": \"george.edwards@reqres.in\",\n" +
                "      \"first_name\": \"George\",\n" +
                "      \"last_name\": \"Edwards\",\n" +
                "      \"avatar\": \"https://s3.amazonaws.com/uifaces/faces/twitter/mrmoiree/128.jpg\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": 12,\n" +
                "      \"email\": \"rachel.howell@reqres.in\",\n" +
                "      \"first_name\": \"Rachel\",\n" +
                "      \"last_name\": \"Howell\",\n" +
                "      \"avatar\": \"https://s3.amazonaws.com/uifaces/faces/twitter/hebertialmeida/128.jpg\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"ad\": {\n" +
                "    \"company\": \"StatusCode Weekly\",\n" +
                "    \"url\": \"http://statuscode.org/\",\n" +
                "    \"text\": \"A weekly newsletter focusing on software development, infrastructure, the server, performance, and the stack end of things.\"\n" +
                "  }\n" +
                "}"
        val json=JSONObject(jsonstring)
        if(jsoncheck(json,"page")){
            model.page=json.getInt("page")

      }
        if(jsoncheck(json,"per_page")) {
        model.perPage=json.getInt("per_page")
            d("perpage",model.perPage.toString())
        }
        if(jsoncheck(json,"total")) {
            model.total=json.getInt("total")

        }
        if(jsoncheck(json,"total_pages")) {
            model.totalPages=json.getInt("total_pages")
        }
        if(jsoncheck(json,"data")) {
            var array= json.getJSONArray("data")


            for (index in 0 until array.length()) {

                    var datajs=array.getJSONObject(index)
                   datalist.add(MyModel.data(datajs.getInt("id"),datajs.getString("email"),datajs.getString("first_name"),datajs.getString("last_name"),datajs.getString("avatar")))

            }
        }

        if(jsoncheck(json,"ad")){
            var adjs=json.getJSONObject("ad")
            admodel.company=adjs.getString("company")
            admodel.text=adjs.getString("text")
            admodel.url=adjs.getString("url")
            adlist.add(admodel)
            model.adlist=adlist
            d("ddd",admodel.url)
        }

        Company.setText(admodel.company)
        Url.setText("url: "+admodel.url)
        text.setText(admodel.text.substring(0 ,admodel.text.length-30)+"...")

        adapter= Adapter(datalist)
        recycle.layoutManager=GridLayoutManager(this,2)
        recycle.adapter=adapter



        }



    fun jsoncheck(jsonobject:JSONObject,key:String):Boolean{

        if(!jsonobject.has(key)) return false
        return true

    }

}
